{ ... }:
{
  imports = [./kitty
             # ./zsh
             ./fish 
             ./starship
             ./exa
             ./alacritty
            ];


#  programs.kitty = {
#    enable = true;
#    settings = {
      # font_family        = "Source Code Pro Semibold";
      #font_family        = "mononoki Nerd Font mono";
#      font_family        = "Consolas";
#      bold_font          = "auto";
#      italic_font        = "auto";
#      bold_italic_font   = "auto";
#      font_size          = "21.0";
#      background_opacity = "1.0";
#			include            = "~/aios/programs/kitty/themes/Dracula.conf";
			#include            = "./Borland.conf";
 #   };
 # };

}
