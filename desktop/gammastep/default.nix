{  config, lib, pkgs, ... }:{
  services.gammastep = {
    enable = true;
    latitude = 17.5;
    longitude = 78.3;
    settings = {
      general = {
        brightness-day = "0.86";
        brightness-night = "0.58";
        gamma = "0.8";
      };
    };
    temperature = {
      day = 3400;
      night = 3400;
    }; 
  };
}
