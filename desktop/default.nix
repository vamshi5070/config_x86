{ config, lib, pkgs, ... }:
{
    imports = [./xmobar.nix
	       ./xmonad.nix
              ./battery
               ./gammastep
               ./playerctl
              ./notifications
	      ];

}
