(set-face-attribute 'font-lock-comment-face nil
	:slant 'italic)
(add-to-list 'load-path "~/config/dev/emacs/emux/")
(require 'emux-packages)
(require 'emux-nano)

(require 'emux-config)

(setq user-full-name "Vamshi krishna"
      user-mail-address "vamshi5070k@gmail.com")

(require 'emux-tab-bar)
(global-set-key (kbd "C-c C-n") 'tab-next)
(require 'emux-appearance)

(require 'emux-org)

(require 'emux-littering)

(require 'emux-window)
(require'emux-dired)
(require 'emux-buffer)

(use-package magit
  :straight t)
(use-package white-theme
  :straight t)
;; (load-theme 'white t)

(require 'emux-languages)

(require 'emux-envrc)
;; (use-package magit
;; :straight t)

(require 'emux-multiple-cursors)
 ;; (grep-apply-setting
 ;;   'grep-find-command
 ;;   '("rg -n -H --no-heading -e '' $(git rev-parse --show-toplevel || pwd)" . 27)
 ;; )
 ;; (global-set-key (kbd "C-x C-g") 'grep-find)

(require 'emux-pdf)

;;  (add-path
;;   (add-to-list 'load-path "~/aios/editor/emacs/emux/")
(require 'emux-keybindings)


