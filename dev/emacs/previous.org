* Looks:
#+ATTR_ORG: :width 500
[[file: ./images/first.jpg]]
* Load path
#+begin_src emacs-lisp
(add-to-list 'load-path "~/nixos-m1/emacs")
#+end_src
* Config
#+begin_src emacs-lisp
  (defun reload ()
   (interactive )
  (load-file (expand-file-name "~/.emacs.d/init.el")))

  (defun private-config ()
	(interactive )
  (find-file (expand-file-name "~/nixos-m1/emacs/README.org")))
   ;; (require 'emux-minibuffer)
#+end_src
* Visualine
#+begin_src emacs-lisp
(global-visual-line-mode t)
  ;; (require 'emux-minibuffer)
#+end_src
* IO
#+begin_src emacs-lisp

    (defvar bootstrap-version)
    (let ((bootstrap-file
	   (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
	  (bootstrap-version 5))
      (unless (file-exists-p bootstrap-file)
	(with-current-buffer
	    (url-retrieve-synchronously
	     "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	     'silent 'inhibit-cookies)
	  (goto-char (point-max))
	  (eval-print-last-sexp)))
      (load bootstrap-file nil 'nomessage))


    (setq package-list
	  '(
	    ;; cape                ; Completion At Point Extensions
  ;;	  orderless           ; Completion style for matching regexps in any order
  ;;	  vertico             ; VERTical Interactive COmpletion
	    ;; marginalia          ; Enrich existing commands with completion annotations
  ;;	  consult             ; Consulting completing-read
	    ;; corfu               ; Completion Overlay Region FUnction
	    ;; deft                ; Quickly browse, filter, and edit plain text notes
	    ;; elfeed              ; Emacs Atom/RSS feed reader
	    ;; elfeed-org          ; Configure elfeed with one or more org-mode files
	    ;; citar               ; Citation-related commands for org, latex, markdown
	    ;; citeproc            ; A CSL 1.0.2 Citation Processor
	    ;; flyspell-correct-popup ; Correcting words with flyspell via popup interface
	    ;; flyspell-popup      ; Correcting words with Flyspell in popup menus
	    ;; guess-language      ; Robust automatic language detection
	    ;; helpful             ; A better help buffer
	    ;;haskell-mode
	   ;; nix-mode
  ;;	 ;; rust-mode
	    ;; cider
  ;;	  clojure-mode
  ;;	  solarized-theme
	    ;; evil
	    ;; org
	    ;; org-babel
  ;;	  org-modern
  ;;	  ace-window
  ;;	  corfu
  ;;	  cape	
	    ;;magit
  ;;	  mini-frame          ; Show minibuffer in child frame on read-from-minibuffer
	    envrc   ; envrc
	    ;; imenu-list          ; Show imenu entries in a separate buffer
	    ;; magit               ; A Git porcelain inside Emacs.
	    ;; markdown-mode       ; Major mode for Markdown-formatted text
	    ;; multi-term          ; Managing multiple terminal buffers in Emacs.
	    ;; pinentry            ; GnuPG Pinentry server implementation
	    ;; use-package         ; A configuration macro for simplifying your .emacs
	    ;; which-key
	))         ; Display available keybindings in popup

    ;; Install packages that are not yet installed
    (dolist (package package-list)
      (straight-use-package package))
#+end_src
* Defaults
#+begin_src emacs-lisp
  (if (boundp 'use-short-answers)
      (setq use-short-answers t)
    (defalias 'yes-or-no-p 'y-or-n-p))
#+end_src
* Envrc
#+begin_src emacs-lisp
  (envrc-global-mode 1)
#+end_src
* Coding system
#+begin_src emacs-lisp
  (prefer-coding-system 'utf-8)	        ;; Encoding
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
#+end_src
* Appearance
#+begin_src emacs-lisp
  (set-face-attribute 'default nil
			:font "Source Code Pro"
				       :height 120
				       :weight 'regular)

       (set-face-attribute 'fixed-pitch nil
			     :font "Source Code Pro"
			   :height 120
			    :weight 'regular)

       (set-face-attribute 'variable-pitch nil
			     :font "Source Code Pro"
			   :height 120
			    :weight 'regular)
  (setq modus-themes-italic-constructs t
	    modus-themes-bold-constructs nil
	    ;; modus-themes-variable-pitch-ui t
	   modus-themes-region '(bg-only no-extend)
	    modus-themes-hl-line '(accented) 
	    modus-themes-syntax '(yellow-comments)
	    modus-themes-mode-line '(accented 3d borderless))
  ;; Color modeline in active window, remove border
     (setq modus-themes-headings ;; Makes org headings more colorful
	   '((t . (rainbow))))
  (load-theme 'modus-vivendi t)

  (global-hl-line-mode 1)
#+end_src
* Dired
#+begin_src emacs-lisp
  (require 'dired)

	(put 'dired-find-alternate-file 'disabled nil)
  ;; Old alternative for dired-kill-when-opening-new-dired-buffer option.
   (add-hook 'dired-mode-hook 'dired-hide-details-mode)
   ;; (add-hook 'dired-mode-hook 'dired-omit-mode)
  ;; (setq-default dired-recursive-copies 'top   ;; Always ask recursive copy
  ;; 	      dired-recursive-deletes 'top  ;; Always ask recursive delete
  ;; 	      dired-dwim-target t	    ;; Copy in split mode with p
  ;; 	      dired-auto-revert-buffer t
  ;; 	      dired-listing-switches "-alh -agho --group-directories-first"
  ;; 	      dired-kill-when-opening-new-dired-buffer t ;; only works for emacs > 28
  ;; 	      dired-isearch-filenames 'dwim
  ;; 	      dired-omit-files "^\\.[^.].*"
  ;; 	      dired-omit-verbose nil
  ;; 	      dired-hide-details-hide-symlink-targets nil
  ;; 	      delete-by-moving-to-trash t
  ;; 	      )
  (define-key dired-mode-map (kbd "j") 'dired-next-line)
       (define-key dired-mode-map (kbd "k") 'dired-previous-line)
       (define-key dired-mode-map (kbd "h") 'dired-up-directory)
       (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
	(define-key dired-mode-map (kbd "/") 'dired-goto-file)
#+end_src
* Window
#+begin_src emacs-lisp
    (defun emux/horizontal-split ()
       (interactive )
       (split-window-below)
       (other-window 1))

    (defun emux/vertical-split ()
       (interactive )
       (split-window-right)
       (other-window 1))

    (defun emux/up-window ()
       (interactive )
       (other-window -1))
    (defun emux/down-window ()
       (interactive )
       (other-window 1))
#+end_src
* Isearch
#+begin_src emacs-lisp
  ;; Isearch

  (setq-default search-nonincremental-instead nil    ;; No incremental if enter & empty
		lazy-highlight-no-delay-length 1     ;; normal delay
		;; lazy-highlight-initial-delay 0
		isearch-allow-scroll t 	           ;; Permit scroll can be 'unlimited
		isearch-lazy-count t
		search-ring-max 256
		regexp-search-ring-max 256
		isearch-yank-on-move 'shift          ;; Copy text from buffer with meta
		;; isearch-wrap-function #'ignore     ;; Look at the emacs-major-version check
		;; isearch-wrap-pause t               ;; Disable wrapping nil.
		isearch-repeat-on-direction-change t ;; Don't go to the other end on direction change
		isearch-lax-whitespace t
		isearch-regexp-lax-whitespace t      ;; swiper like fuzzy search
		search-whitespace-regexp ".*?"
		;; Emacs version > 28
		lazy-highlight-no-delay-length 1     ;; use this instead of lazy-highlight-initial-delay
		isearch-allow-motion t
		isearch-forward-thing-at-point '(region symbol sexp word)
		;; isearch-motion-changes-direction t
		)

   (defun my/isearch-exit-other-end ()
      (interactive)
      (when isearch-other-end
	(goto-char isearch-other-end))
      (call-interactively #'isearch-exit))

    (define-key isearch-mode-map (kbd "C-RET") #'my/isearch-exit-other-end)
    (define-key isearch-mode-map (kbd "C-<return>") #'my/isearch-exit-other-end)
    (define-key isearch-mode-map [remap isearch-abort] #'isearch-cancel)
    (define-key isearch-mode-map [remap isearch-delete-char] #'isearch-del-char)
    (define-key search-map "." #'isearch-forward-thing-at-point)
    ;; )
#+end_src
* History
#+begin_src emacs-lisp

  ;; Save history
  (savehist-mode t)
  ;; saveplace
  (save-place-mode 1)                           ;; Remember point in files
  (setq save-place-ignore-files-regexp  ;; Modified to add /tmp/* files
	(replace-regexp-in-string "\\\\)\\$" "\\|^/tmp/.+\\)$"
				  save-place-ignore-files-regexp t t))

  (recentf-mode 1)
  (setq recentf-max-menu-items 25)
  (setq recentf-max-saved-items 25)
  (global-set-key "\C-x\ \C-r" 'recentf-open-files)
#+end_src
* Modeline
#+begin_src emacs-lisp
  ;;__________________________________________________________
  ;; mode-line
  ;; (setq-default mode-line-position-column-line-format '(" (%l,%C)")  ;; column number start on 1
  ;;               ;; mode-line-compact t                                  ;; no spaces on ml
  ;; 	      mode-line-frame-identification " "                   ;; no F1 in term
  ;;               mode-line-front-space " "                            ;; no - on the very left
  ;;               mode-line-end-spaces " "                             ;; no ---- on the right.
  ;;               mode-line-mule-info ""                               ;; no UUU: on the left.
  ;;               )

  (setq-default mode-line-format 
		(list  "  " mode-line-modified "  | %m:" " %b |"   " %l ," "%C |"   )

  )
#+end_src
* Linum
#+begin_src emacs-lisp
  (column-number-mode 1)

  (global-display-line-numbers-mode 1)
       (setq display-line-numbers-type 'relative)
       (dolist (mode '( org-mode-hook
			 prog-mode-hook
			 ;; term-mode-hook
			 ;; vterm-mode-hook
			 ;; shell-mode-hook
			 ;; dired-mode-hook
			 ;; eshell-mode-hook
			 ))
   (add-hook mode (lambda () (display-line-numbers-mode 1))))
#+end_src
* Littering
#+begin_src emacs-lisp

  (setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

  (make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

  (setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
	auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

  (setq create-lockfiles nil)
#+end_src
* Minibuffers
#+begin_src emacs-lisp
    ;; (require 'io-minibuffer)

  ;;__________________________________________________________
  ;; minibuffers

  ;; These two must be enabled/disabled together
  (setq-default ;; enable-recursive-minibuffers t     ;; Enable nesting in minibuffer
		completion-show-help nil           ;; Don't show help in completion buffer
		completion-auto-help 'lazy
		completion-auto-select t
		completion-wrap-movement t
		completions-detailed t             ;; show more detailed completions
		completions-format 'one-column     ;; Vertical completion list
		completions-max-height 15
		completion-styles '(substring partial-completion emacs22)
		;; M-x show context-local commands
		read-extended-command-predicate #'command-completion-default-include-p
		read-file-name-completion-ignore-case t
		read-buffer-completion-ignore-case t
		completion-ignore-case t)

  ;; These two must be enabled/disabled together
  ;; (setq-default enable-recursive-minibuffers t) ;; Enable nesting in minibuffer
  ;; (minibuffer-depth-indicate-mode 1)            ;; Mostrar nivel de nesting en minibuffer

  ;; (setq minibuffer-eldef-shorten-default t)
  (add-hook 'minibuffer-setup-hook #'my/unset-gc)
  (add-hook 'minibuffer-exit-hook #'my/restore-gc)

  ;; Arrows up/down search prefix in history like `history-search-backward' in bash
  ;; (keymap-set minibuffer-local-map "<down>" #'next-complete-history-element)
  ;; (keymap-set minibuffer-local-map "<up>" #'previous-complete-history-element)

  (defun my/completion-setup-hook ()
    "My hook for Completions window."
    (with-current-buffer standard-output
      (setq-local mode-line-format nil)
      (display-line-numbers-mode -1)))

  (add-hook 'completion-setup-hook #'my/completion-setup-hook 10)

#+end_src
* Languages
#+begin_src emacs-lisp
   ;; (require 'io-languages)
  ;; (require 'emux-minibuffer)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
		  (setq  haskell-interactive-popup-errors nil)
#+end_src
* Org
#+begin_src emacs-lisp
     ;; (require 'emux-org)
    ;; (require 'emux-minibuffer)
  (setq org-ellipsis ;; " ▼ ") ;; ⤵
  "…")	
	(add-hook 'org-mode-hook #'org-shifttab)
  

  ;; Minimal UI
  ;;(package-initialize)
  ;;(menu-bar-mode -1)
  ;;(tool-bar-mode -1)
  ;;(scroll-bar-mode -1)
  ;;(modus-themes-load-operandi)

  ;; Choose some fonts
  ;; (set-face-attribute 'default nil :family "Iosevka")
  ;; (set-face-attribute 'variable-pitch nil :family "Iosevka Aile")
  ;; (set-face-attribute 'org-modern-symbol nil :family "Iosevka")

  ;; Add frame borders and window dividers

#+end_src

* C-z
#+begin_src emacs-lisp
(global-set-key (kbd "C-z" ) #'(lambda () (message "pressed C-z")))
#+end_src

* Cape
#+begin_src emacs-lisp
;;  (require 'io-cape)
    ;; (require 'emux-minibuffer)

#+end_src

* Bling
#+begin_src emacs-lisp
   ;; (require 'io-bling)
  ;; (require 'emux-minibuffer)
#+end_src
* ace window
#+begin_src emacs-lisp
  ;; (require 'io-window)
    ;; (require 'emux-minibuffer)
(global-set-key (kbd "M-o") 'ace-window)
#+end_src
* Ghcid
#+begin_src emacs-lisp
   ;; (require 'ghcid)
  ;; (require 'emux-minibuffer)
#+end_src
* Io magit
#+begin_src emacs-lisp

  ;; (add-hook 'magit-mode-hook
  ;;   (lambda ()
  ;;    (local-set-key [j] 'magit-next-line)))

   ;; (define-key magit-status-mode-map (kbd "j") 'magit-next-line)
#+end_src
* Keybindings
#+begin_src emacs-lisp


    (defvar my-subword-forward-regexp "[[[:upper:]]*[[:digit:][:lower:]]+\\|[[:upper:]]+\\|[^[:word:][:space:]_\n]+\\|-|(|")

      (defvar my-subword-backward-regexp "[[:space:][:word:]_\n][^\n[:space:][:word:]_]+\\|\\(\\W\\|[[:lower:]]\\)[[:upper:]]\\|\\W\\w+")
      ;; (defvar my-subword-backward-regexp "[^[:word:][:space:]_\n]+")
      (defun my-subword-forward-internal () ""
	     (interactive)  
	     (let ((case-fold-search nil))
	       (re-search-forward my-subword-forward-regexp nil t))
	     (goto-char (match-end 0)))

      (defun my-subword-backward-internal () ""
	     (interactive)
	     (let ((case-fold-search nil))
	       (if (re-search-backward my-subword-backward-regexp nil "don't panic!")
	       (goto-char (1+ (match-beginning 0))))))

      (setq subword-forward-regexp 'my-subword-forward-regexp)
      (setq subword-backward-regexp 'my-subword-backward-regexp)
      (setq subword-backward-function 'my-subword-backward-internal)
      (setq subword-forward-function 'my-subword-forward-internal)
    ;; (subword-mode 1)


    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	  ;; (setq ryo-modal-cursor-color "#000000")
	  ;;(global-set-key (kbd "<escape>")  'ryo-modal-mode)
	  (defun kakoune-a (count)
	"Select COUNT lines from the current line.
			 Note that kakoune's x does behaves exactly like this,
			 and I like this behavior better."
	(interactive "p")
	(deactivate-mark)
	(forward-char count)
	(ryo-modal-mode 0))

	  (defun kakoune-modal-a (count)
	"Select COUNT lines from the current line.
			 Note that kakoune's x does behaves exactly like this,
			 and I like this behavior better."
	(interactive "p")
	(deactivate-mark)
	(forward-char count)
	(modal-insert-mode))
	;; (toggle-modal))

	  (defun kakoune-set-mark-here () "Set the mark at the location of the point."
	     (interactive) (set-mark (point)))

	  (defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
	     (interactive)
	     (unless (use-region-p) (set-mark (point))))

	  (defun kakoune-deactivate-mark ()
	"Deactivate the mark.
			 Deactivate the mark unless mark-region-mode is active."
	(interactive)
	;;(unless rectangle-mark-mode (deactivate-mark)))
	(deactivate-mark))
	  (defun kakoune-O (count)
	"Open COUNT lines above the cursor and go into insert mode."
	(interactive "p")
	(beginning-of-line)
	(dotimes (_ count)
	  (newline)
	  (forward-line -1)))

	  (defun kakoune-x (count)
	"Select COUNT lines from the current line.
			 Note that kakoune's x does behaves exactly like this,
			 and I like this behavior better."
	(interactive "p")
	(beginning-of-line count)
	(set-mark (point))
	(end-of-line count)
	(forward-char count))

    (defun kakoune-d (count)
	"Kill selected text or COUNT chars."
	(interactive "p")
	(if (use-region-p)
	    (kill-region (region-beginning) (region-end))
	  (delete-char count t)))

	  (defun kakoune-insert-mode () "Return to insert mode."
	      (interactive)
	     (ryo-modal-mode 0))
    ;;(defun kakoune-c ()
     ;; (interactive)
     ;; (kakoune-d )

    (defun kakoune-c (count)
	"Kill selected text or COUNT chars."
	(interactive "p")
	(if (use-region-p)
	    (kill-region (region-beginning) (region-end))
	  (delete-char count t))
	(modal-insert-mode))
      ;; (toggle-modal)


    (defun kakoune-h (count)
      (interactive "p")
      (deactivate-mark)
      (backward-char count)
      )
    (defun kakoune-l (count)
      (interactive "p")
      (deactivate-mark)
      (forward-char count)
      )

    (defun kakoune-j (count)
      (interactive "p")
      (deactivate-mark)
    (next-line count)
      )
    (defun kakoune-k (count)
      (interactive "p")
      (deactivate-mark)
    (previous-line count)
    )
    (defun kakoune-H (count)
      (interactive "p")
      (kakoune-set-mark-if-inactive)
      (backward-char count)
      )
    (defun kakoune-L (count)
      (interactive "p")
      (kakoune-set-mark-if-inactive)
      (forward-char count)
      )

    (defun kakoune-J (count)
      (interactive "p")
      (kakoune-set-mark-if-inactive)
    (next-line count)
      )
    (defun kakoune-K (count)
      (interactive "p")
      (kakoune-set-mark-if-inactive)
    (previous-line count)
    )
    (defun kakoune-w (count)
	(interactive "p")
       (kakoune-set-mark-here)
    (subword-forward count)
      )
    (defun kakoune-e ()
      (interactive)
      (progn
       (kakoune-w)
       (kakoune-H)
      ))
    (defun kakoune-f ()
      (interactive)
      (kakoune-set-mark-here)
      (kakoune-select-to-char))

    (defun kakoune-W (count)
	(interactive "p")
       (kakoune-set-mark-if-inactive)
    (subword-forward count)
      )
    (defun kakoune-B (count)
	(interactive "p")
       (kakoune-set-mark-if-inactive)
    (subword-backward count)
      )
    (defun kakoune-b (count)
	(interactive "p")
       (kakoune-set-mark-here)
    (subword-backward count)
      )
    (defun kakoune-exchange () "Return to insert mode."
	     (interactive)
	     (exchange-point-and-mark))

	  (defun kakoune-normal-mode () "Return to insert mode."
	     (interactive)
	     (ryo-modal-mode 1))

	  (defun ryo-after () "Enter normal mode"
	     (interactive)
	     (forward-char)
	     (ryo-modal-mode 0))

	  (defun kakoune-X (count)
	"Extend COUNT lines from the current line."
	(interactive "p")
	(beginning-of-line)
	(unless (use-region-p) (set-mark (point)))
	(forward-line count))

	  (defun kakoune-M-x (count)
	"Extend COUNT lines from the current line."
	(interactive "p")
	(beginning-of-line)
	(unless (use-region-p) (set-mark (point)))
	(previous-line count))

	  (defun kakoune-gg (count)
	"Go to the beginning of the buffer or the COUNTth line."
	(interactive "p")
	(goto-char (point-min))
	(when count (forward-line (1- count))))

	  (defun kakoune-o (count)
	"Open COUNT lines under the cursor and go into insert mode."
	(interactive "p")
	(end-of-line)
	(dotimes (_ count)
	  (electric-newline-and-maybe-indent))
	(ryo-modal-mode 0))

	  (defun kakoune-modal-o (count)
	"Open COUNT lines under the cursor and go into insert mode."
	(interactive "p")
	(end-of-line)
	(dotimes (_ count)
	  (electric-newline-and-maybe-indent))
	(modal-insert-mode))
	;; (toggle-modal))

    (defun kakoune-modal-i ()
      (interactive )
      (deactivate-mark)
      (modal-insert-mode))

	  (defun kakoune-p (count)
	"Yank COUNT times after the point."
	(interactive "p")
	(forward-line)
	(dotimes (_ count) (save-excursion (yank))))

	  (defun kakoune-P (count)
	"Yank COUNT times after the point."
	(interactive "p")
	(dotimes (_ count) (save-excursion (yank))))

	  (defvar kakoune-last-t-or-f ?f
	"Using t or f command sets this variable.")

	  (defun kakoune-select-to-char (arg char)
	"Select up to, and including ARGth occurrence of CHAR.
		     Case is ignored if `case-fold-search' is non-nil in the current buffer.
		     Goes backward if ARG is negative; error if CHAR not found.
		     Ignores CHAR at point."
	(interactive "p\ncSelect to char: ")
	(setq kakoune-last-char-selected-to char)
	(setq kakoune-last-t-or-f ?f)
	(let ((direction (if (>= arg 0) 1 -1)))
	  (forward-char direction)
	  (unwind-protect
	      (search-forward (char-to-string char) nil nil arg))
	  (point)))

	  (defun kakoune-f (arg char)
	"Select up to, and including ARGth occurrence of CHAR.
		     Case is ignored if `case-fold-search' is non-nil in the current buffer.
		     Goes backward if ARG is negative; error if CHAR not found.
		     Ignores CHAR at point."
	(interactive "p\ncSelect to char: ")
	(kakoune-set-mark-here)
	(setq kakoune-last-char-selected-to char)
	(setq kakoune-last-t-or-f ?f)
	(let ((direction (if (>= arg 0) 1 -1)))
	  (forward-char direction)
	  (unwind-protect
	      (search-forward (char-to-string char) nil nil arg))
	  (point)))

	  (defun kakoune-F (arg char)
	"Select up to, and including ARGth occurrence of CHAR.
		     Case is ignored if `case-fold-search' is non-nil in the current buffer.
		     Goes backward if ARG is negative; error if CHAR not found.
		     Ignores CHAR at point."
	(interactive "p\ncSelect to char: ")
	(kakoune-set-mark-if-inactive)
	(setq kakoune-last-char-selected-to char)
	(setq kakoune-last-t-or-f ?f)
	(let ((direction (if (>= arg 0) 1 -1)))
	  (forward-char direction)
	  (unwind-protect
	      (search-forward (char-to-string char) nil nil arg))
	  (point)))
    (defun kakoune-select-up-to-char (arg char)
	"Select up to, but not including ARGth occurrence of CHAR.
		     Case is ignored if `case-fold-search' is non-nil in the current buffer.
		     Goes backward if ARG is negative; error if CHAR not found.
		     Ignores CHAR at point."
	(interactive "p\ncSelect up to char: ")
	(setq kakoune-last-char-selected-to char)
	(setq kakoune-last-t-or-f ?t)
	(let ((direction (if (>= arg 0) 1 -1)))
	  (forward-char direction)
	  (unwind-protect
	      (search-forward (char-to-string char) nil nil arg)
	    (backward-char direction))
	  (point)))

    (defun kakoune-t (arg char)
	"Select up to, but not including ARGth occurrence of CHAR.
		     Case is ignored if `case-fold-search' is non-nil in the current buffer.
		     Goes backward if ARG is negative; error if CHAR not found.
		     Ignores CHAR at point."
	(interactive "p\ncSelect up to char: ")
	(kakoune-set-mark-here)
	(setq kakoune-last-char-selected-to char)
	(setq kakoune-last-t-or-f ?t)
	(let ((direction (if (>= arg 0) 1 -1)))
	  (forward-char direction)
	  (unwind-protect
	      (search-forward (char-to-string char) nil nil arg)
	    (backward-char direction))
	  (point)))


    (defun kakoune-T (arg char)
	"Select up to, but not including ARGth occurrence of CHAR.
		     Case is ignored if `case-fold-search' is non-nil in the current buffer.
		     Goes backward if ARG is negative; error if CHAR not found.
		     Ignores CHAR at point."
	(interactive "p\ncSelect up to char: ")
	(kakoune-set-mark-if-inactive)
	(setq kakoune-last-char-selected-to char)
	(setq kakoune-last-t-or-f ?t)
	(let ((direction (if (>= arg 0) 1 -1)))
	  (forward-char direction)
	  (unwind-protect
	      (search-forward (char-to-string char) nil nil arg)
	    (backward-char direction))
	  (point)))
    (defun kakoune-word-backward ()
	(interactive)
	;; (progn (forward-char)
	;;     (forward-word))
	(subward-backward
	 ))

	  (defun kakoune-word-forward ()
	(interactive)
	;; (progn (forward-char)
	;;     (forward-word))
	(forward-word
	 ))

	  (defun kakoune-forward-char ()
	(interactive)
	(forward-char))

	  (defun kakoune-A (count)
	"Yank COUNT times after the point."
	(interactive "p")
	(end-of-line)
	(ryo-modal-mode 0))

	  (defun kakoune-modal-A (count)
	"Yank COUNT times after the point."
	(interactive "p")
	(end-of-line)
	(modal-insert-mode))
	;; (toggle-modal))

	(defun kakoune-downcase ()
	  "Downcase region."
	  (interactive)
	  (if (use-region-p)
	  (downcase-region (region-beginning) (region-end))
	(downcase-region (point) (+ 1 (point)))))

	(defun kakoune-upcase ()
	  "Upcase region."
	  (interactive)
	  (if (use-region-p)
	  (upcase-region (region-beginning) (region-end))
	(upcase-region (point) (1+ (point)))))

	(defun kakoune-replace-char (char)
	"Replace selection with CHAR."
	(interactive "cReplace with char: ")
	(mc/execute-command-for-all-cursors
	 (lambda () (interactive)
	   (if (use-region-p)
	   (progn (let ((region-size (- (region-end) (region-beginning))))
		(delete-region (region-beginning) (region-end))
		(mc/save-excursion
		     (insert-char char region-size t))))
	 (progn (delete-region (point) (1+ (point)))
		(mc/save-excursion
		 (insert-char char)))))))

      (defun kakoune-replace-selection ()
	"Replace selection with killed text."
	(interactive)
	(if (use-region-p)
	(progn (delete-region (region-beginning) (region-end))
	       (yank))
	  (progn (delete-region (point) (1+ (point)))
	     (yank))))

      (defun kakoune-insert-line-below (count)
	"Insert COUNT empty lines below the current line."
	(interactive "p")
	(save-excursion
	  (end-of-line)
	  (open-line count)))

      (defun kakoune-insert-line-above (count)
	"Insert COUNT empty lines above the current line."
	(interactive "p")
	(save-excursion
	  (end-of-line 0)
	  (open-line count)))

      (defun kakoune-paste-above (count)
	"Paste (yank) COUNT times above the current line."
	(interactive "p")
	(save-excursion
	  (dotimes (_ count) (end-of-line 0)
	       (newline)
	       (yank))))

      (defun kakoune-paste-below (count)
	"Paste (yank) COUNT times below the current line."
	(interactive "p")
	(save-excursion
	  (dotimes (_ count) (end-of-line)
	       (newline)
	       (yank))))
    ;; (provide 'kakoune)
    ;;   (require 'emux-keybindings)
  
  (subword-mode 1)

  (define-minor-mode modal
		      "Docstring"
		     ;; initial value
		      :init-value
		      nil
		     ;; indicator for mode line
		      :lighter	   " modal"
		      :keymap
		      ;; :after-hook  (message "rgv")
	     '(
	     ((kbd ";") . kakoune-deactivate-mark)
	     ;; ("0" beginning-of-line )
	     ((kbd "%") . mark-whole-buffer)
			       ;; ((kbd "i") . toggle-my-cummand-mode)
			       ((kbd "a") . kakoune-modal-a)
			       ;; ((kbd " u") . undo-tree-visualize)
			       ((kbd "/") . isearch-forward)
			       ((kbd "b") . kakoune-b)
			       ((kbd " ff") . find-file)
			       ((kbd " ts") . tab-switch)
			       ((kbd " sg") . grep)
			       ((kbd " sr") . rgrep)
			       ;; ((kbd "s") . cape-line)    
			       ((kbd " wv") . emux/vertical-split)
			       ((kbd " ot") . vterm)
			       ((kbd " wf") . find-file);;ido-find-file-other-window)
			       ((kbd " wd") . delete-window)
			       ((kbd " bb") . ibuffer)
			       ((kbd " rs") . replace-string)
			       ;; ((kbd " bb") . emux/buffer/body)
			       ((kbd " ws") . emux/horizontal-split)
			       ((kbd " wa") . delete-other-windows)
			       ((kbd " ww") . other-window)
			       ((kbd " wj") . emux/down-window)
			       ((kbd " wk") . emux/up-window)
			       ((kbd " ep") . private-config)
			       ((kbd " er") . reload)
			       ;; ((kbd " et") . consult-theme)
			       ((kbd " fr") . consult-recent-file)
			       ((kbd " fs") . save-buffer)
			       ((kbd " fd") . dired-jump)
			       ((kbd " ss") . consult-line)
			       ((kbd " pp") . project-find-file)
			       ((kbd " pP") . project-switch-project)
			       ((kbd "c") .  kakoune-c )
			       ((kbd "d") .  kakoune-d)
			       ;; ((kbd "e") .  kakoune-e)
			       ((kbd "f") .  kakoune-f)
			       ((kbd "h") . kakoune-h)
			       ((kbd "gc") . comment-dwim )
			       ((kbd "gg") . kakoune-gg )
			       ((kbd "gh") . beginning-of-line )
			       ((kbd "gj") . end-of-buffer )
			       ((kbd "gk") . beginning-of-buffer )
			       ((kbd "gl") . end-of-line )
			       ((kbd "gn") . next-error )
			       ((kbd "gp") . previous-error )
			       ((kbd "i") . kakoune-modal-i)
			       ((kbd "j") . kakoune-j)
			       ((kbd "k") . kakoune-k)
			       ((kbd "l") . kakoune-l)
			       ((kbd "p") .  kakoune-p)
			       ;; ((kbd "p") .  kakoune-p)
			       ((kbd "o") . kakoune-modal-o)
			       ((kbd "r") . kakoune-replace-char)
			       ((kbd "t") .  kakoune-t)
			       ((kbd "u") . undo)
			       ((kbd "w") . kakoune-w)
			       ((kbd "x") . kakoune-x)
			       ((kbd "y") . kill-ring-save)
			       ((kbd "A") . kakoune-modal-A)
			       ((kbd "B") . kakoune-B)
			       ((kbd "F") . kakoune-F)
			       ((kbd "H") . kakoune-H)
			       ((kbd "I") . toggle-modal)
			       ((kbd "J") . kakoune-J)
			       ((kbd "K") . kakoune-K)
			       ((kbd "L") . kakoune-L)
			       ((kbd "O") . kakoune-O)
			       ((kbd "P") . kakoune-P)
			       ((kbd "R") . kakoune-replace-selection)
			       ((kbd "T") . kakoune-T)
			       ((kbd "U") . undo-redo)
			       ((kbd "W") . kakoune-W)
			       ((kbd "X") . kakoune-X)
			       ((kbd "1") . [?\C-1])
			       ((kbd "2") . [?\C-2])
			       ((kbd "3") . [?\C-3])
			       ((kbd "4") . [?\C-4])
			       ((kbd "5") . [?\C-5])
			       ((kbd "6") . [?\C-6])
			       ((kbd "7") . [?\C-7])
			       ((kbd "8") . [?\C-8])
			       ((kbd "9") . [?\C-9])
			       ((kbd "0") . [?\C-0])
			     ;; :group 'mcm-group
			     ))
    (define-key modal-map  (kbd "M-;") #'kakoune-exchange)
    (define-key modal-map (kbd "<RET>") #'next-line)
    (define-key modal-map (kbd "<backspace>") #'kakoune-h )

    ;; (define-key modal-map (kbd " <tab>") #'tab-switch)
    ;; (define-key modal-map (kbd "SPC-<tab>") #'tab-switch)
	    ;;(global-set-key [?\H-a] 'find-file)
			 (defun toggle-modal (&optional set-state)
			   "Toggle `my-command-mode', optionally ensuring its state with `SET-STATE'.

			 `SET-STATE' is interpreted as follows:
			   nil   (Same as no argument) Toggle `my-command-mode'
			   -1    Ensure `my-command-mode' is disabled
			   else  Ensure `my-command-mode' is enabled
			 "
			    (interactive )
			   (cond ((equal set-state -1)
				  (when modal
				    (modal -1)))

				 ((equal set-state nil)
				  (modal (if modal -1 1)))

				 (else
				  (unless modal
				    (modal 1)))))

	      (defun modal-normal-mode ()
		(interactive)
    ;; (setq-default cursor-type '(hbar . 0 ))
	      (setq cursor-type 'box) 
    ; Set cursor color to white
   ;; (set-cursor-color "#ffffff") 
		;; (message "Entered normal mode")
		(modal 1))

	      (defun modal-insert-mode ()
		(interactive)
    ;;(set-cursor-color "blue")
    (setq cursor-type '(hbar .  2))
	      ;; (setq-default cursor-type 'bar)
		;; (message "Entered insert mode")
		(modal -1))

	(global-set-key (kbd "<escape>") 'modal-normal-mode)
			     ;; :group '-group)
			   ;; )
      (add-hook 'prog-mode-hook #'modal-normal-mode)
      (add-hook 'org-mode-hook #'modal-normal-mode)
	    (modal-normal-mode)
#+end_src


* Io bling
#+begin_src emacs-lisp
  
 (vertico-mode)
	    (setq vertico-count 10
		  vertico-cycle t)

	(define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
	(define-key vertico-map (kbd "C-j") #'vertico-next)
	(define-key vertico-map (kbd "C-k") #'vertico-previous)
	;; (define-key vertico-map (kbd "M-h") #'rational-completion/minibuffer-backward-kill)

       (setq completion-styles '(orderless)
	     completion-category-defaults nil
	     completion-category-overrides '((file (styles partial-completion))))

    (global-set-key (kbd "C-x b") 'consult-buffer)
#+end_src
