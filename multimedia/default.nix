{ config, lib, pkgs, ... }:
{
  imports = [
    ./mpv
	      ];
  home.packages = with pkgs;
    [obs-studio];

}
