  (require 'dired)
      (put 'dired-find-alternate-file 'disabled nil)
		 ;;(evil-define-key 'normal dired-mode-map
	    ;;		(kbd "M-RET") 'dired-display-file
	    ;;		(kbd "h") 'dired-up-directory
	    ;;		(kbd "l") 'dired-find-alternate-file ); use dired-find-file instead of dired-open.
	  ;; (defun dired-config ()
	;; "my dired config"
	;; (dired-hide-details-mode 1)
	;;     )
	 (add-hook 'dired-mode-hook 'dired-hide-details-mode)
	 (add-hook 'dired-mode-hook 'dired-omit-mode)

      (setq dired-listing-switches "-ahl -agho --group-directories-first"
	dired-omit-files "^\\.[^.].*"
	dired-omit-verbose nil
	dired-hide-details-hide-symlink-targets nil
	delete-by-moving-to-trash t)
      (define-key dired-mode-map (kbd "j") 'dired-next-line)
     (define-key dired-mode-map (kbd "k") 'dired-previous-line)
     (define-key dired-mode-map (kbd "h") 'dired-up-directory)
     (define-key dired-mode-map (kbd "l") 'dired-find-alternate-file)
      (define-key dired-mode-map (kbd "/") 'dired-goto-file)
      ;; :bind (:map dired-mode-map
      ;; 		("/" . dired-goto-file)
      ;; 		("j" . dired-next-line )
      ;; 		("h" . dired-up-directory )
      ;; 		("k" . dired-previous-line)
      ;; 		("l" . dired-find-alternate-file)
      ;; 		))

    ;; y dired-mode-map (kbd "/") ')
    ;;   (define-key dired-mode-map (kbd "j") 'dired-next-line)
    ;;   (define-key dired-mode-map (kbd "k") ')
    ;;   (define-key dired-mode-map (kbd "h") ')
    ;;   (define-key dired-mode-map (kbd "l") 'dired-)
    ;; )

      ;; (define-key dired-mode-map (kbd "j") 'dired-next-line)
	  ;;  (ryo-modal-major-mode-keys
	   ;; 'dired-mode-hook
	   ;; ("h" dired-up-directory )
	   ;; ("l" dired-find-alternate-file ))

  (provide 'emux-dired)
