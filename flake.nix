{
  description = "Home manager flake";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    custom-taffybar = {
      url = "gitlab:vamshi5070/custom-taffybar";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    lambda-launcher.url = "github:balsoft/lambda-launcher";
    kmonad = {
      url = "github:kmonad/kmonad?dir=nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    taffybar = {
      url = "github:/taffybar/taffybar";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
  };
  outputs = inputs@{ self, nixpkgs, ... }:

    let
      system = "x86_64-linux";

      pkgs = import nixpkgs {

        inherit system;

        config = { allowUnfree = true; };
      };

      lib = nixpkgs.lib;

      nurNoPkgs = import inputs.nur {
        pkgs = null;
        nurpkgs = pkgs;
      };
    in {
      homeConfigurations = {
        cosmos = inputs.home-manager.lib.homeManagerConfiguration {
          pkgs = import inputs.nixpkgs {
            system = "x86_64-linux";
            config.allowUnfree = true;
          };
          system = "x86_64-linux";
          homeDirectory = "/home/vamshi";
          username = "vamshi";
          configuration = { dmenu, config, lib, pkgs, ... }: {
            nixpkgs.overlays = with inputs; [
              emacs-overlay.overlay
              nur.overlay
              kmonad.overlay
              neovim-nightly-overlay.overlay
            ];
            nixpkgs.config = { allowUnfree = true; };
            # This value determines the Home Manager release that your
            # configuration is compatible with. This helps avoid breakage
            # when a new Home Manager release introduces backwards
            # incompatible changes.
            #
            # You can update Home Manager without changing this value. See
            # the Home Manager release notes for a list of state version
            # changes in each release.
            # home.stateVersion = "21.05";
            # home.keyboard = null;
            # Let Home Manager install and manage itself.
            programs = { home-manager.enable = true; };
            imports = [

              #   nurNoPkgs.repos.rycee.hmModules.emacs-init
              # ./emacs
              ./blender
              ./bling
              ./pcmanfm
              # ./notifications
              ./dev
              # ./neovim
              # ./git
              # ./nixfmt
              ./multimedia
              #   ./gtk
              ./desktop
              ./unzip
              #              ./editor/kakoune
              #             ./editor/emacs
              #   ./books
              #   ./desktop
              #             ./programs/firefox
              #             ./programs/htop
              ./timidity
              #             ./excess/gammastep
              #    ./editor/neovim
              #   ./genPackages
              #             ./git
              #  ./multimedia
              ./firefox
              ./direnv
              #              ./notifications
              ./spotify
              ./terminal
              #            ./services/unclutter
            ];
          };
        };
      };

      packages.x86_64-linux =
        builtins.mapAttrs (_name: config: config.activationPackage)
        self.homeConfigurations;

      apps.x86_64-linux = builtins.mapAttrs (name: activationPackage: {
        type = "app";
        program = "${activationPackage}/activate";
      }) self.packages.x86_64-linux;
    };
}
