{pkgs,...}:
{
  programs.htop.enable = true;
  home.packages = with pkgs; [
    xournalpp
    zstd
    neofetch
    libreoffice
    gnome.gnome-disk-utility
    transmission-gtk
    brave
  ];
}
