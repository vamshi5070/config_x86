{pkgs,...}:
{
  home.packages = with pkgs; [
    pcmanfm
    brightnessctl
    ];
}
