(setq gc-cons-percentage 0.6)

(setq inhibit-startup-message t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(fset 'yes-or-no-p 'y-or-n-p)

(defun privateConfig ()
		(interactive )
		 (find-file (expand-file-name "~/aios/editor/emacs/README.org")))

;; (load-theme 'modus-vivendi t)

(column-number-mode t)

(defun reload ()
	       (interactive )
		(load-file (expand-file-name "~/.emacs.d/init.el")))

(setq backup-directory-alist `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)

(setq auto-save-list-file-prefix (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
      auto-save-file-name-transforms `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

(setq create-lockfiles nil)

(set-face-attribute 'default nil
				 :font " Mononoki Nerd Font Mono "
				 :height 200
				 :weight 'regular)

 (set-face-attribute 'variable-pitch nil
			 :font "Agave Nerd Font Mono"
		       :height 160
			:weight 'regular)
 (set-face-attribute 'fixed-pitch nil
		       :font "Agave Nerd Font Mono"
		       :height 200
		       :weight 'regular)
  ;; Uncomment the following line if line spacing needs adjusting.
  (setq-default line-spacing 0.19)

   ;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
  ;; (add-to-list 'default-frame-alist '(font . "Consolas-17"))

 (set-face-attribute 'font-lock-comment-face nil
   :slant 'italic)
 (set-face-attribute 'font-lock-keyword-face nil
		     :slant 'italic)

(defvar bootstrap-version)
(let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)

(straight-use-package '(setup :type git :host nil :repo "https://git.sr.ht/~pkal/setup"))
(require 'setup)

(straight-use-package 'use-package)

;;(setup (:pkg vertico)
			 ;; :straight '(vertico :host github
			 ;;                     :repo "minad/vertico"
			 ;;                     :branch "main")
			;; (vertico-mode))
		    (use-package vertico
		      :straight t
		      :init
		      (vertico-mode))
	  (setq vertico-count 14
		vertico-cycle t)

(define-key vertico-map (kbd "<escape>") #'keyboard-escape-quit)
	       ;;    :bind (("C-j" vertico-next)
	       ;;           ("C-k" vertico-previous)
	       ;;           ("C-f" vertico-exit)))

;; Configure directory extension.
;; (use-package vertico-directory
;;   :straight t
;;   :ensure nil
;;   ;; More convenient directory navigation commands
;;   :bind (:map vertico-map
;;               ("RET" . vertico-directory-enter)
;;               ("DEL" . vertico-directory-delete-char)
;;               ("M-DEL" . vertico-directory-delete-word))
;;   ;; Tidy shadowed file names
;;   :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package orderless
:straight t
:init
;; Configure a custom style dispatcher (see the Consult wiki)
;; (setq orderless-style-dispatchers '(+orderless-dispatch)
;;       orderless-component-separator #'orderless-escapable-split-on-space)
(setq completion-styles '(orderless)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion)))))

(use-package consult
  :straight t)

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
    :init
  (savehist-mode))

;; (setq-default header-line-format mode-line-format)

;; 		  (straight-use-package 'hide-mode-line)
;;     (global-hide-mode-line-mode 1)

;; (set-face-attribute 'mode-line nil :font "Consolas" :height 190)  
;; (set-face-attribute 'mode-line-inactive nil :font "Consolas" :height 110)    ;; (setq-default mode-line-format nil)

(use-package nix-mode
    :straight t
    :mode "\\.nix\\'")

(use-package magit
      :straight t)

(use-package haskell-mode
  :straight t)
;;        :config
;;;;        (load "haskell-mode-autoloads"))
(add-hook 'haskell-mode-hook 'interactive-haskell-mode)
(setq  haskell-interactive-popup-errors nil)

;;(use-package company
  ;;  :straight t)
 ;; (global-company-mode)

(use-package company
  :straight t
  :ensure t
  :config
  (add-to-list 'company-backends 'company-capf)
  (global-company-mode))
 ;;(setq ac-ignore-case nil)

(global-set-key (kbd "C-x b") 'consult-buffer)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  kakoune  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    (setq-default cursor-type 'bar) 
		(setq ryo-modal-cursor-color "lightblue")
		 ;;(global-set-key (kbd "<escape>")  'ryo-modal-mode)
		  (defun kakoune-a (count)
		    "Select COUNT lines from the current line.
		  Note that kakoune's x does behaves exactly like this,
		  and I like this behavior better."
		    (interactive "p")
		    (set-mark (point))
		    (forward-char count)
		    (ryo-modal-mode 0))

		  (defun kakoune-set-mark-if-inactive () "Set the mark if it isn't active."
			 (interactive)
			 (unless (use-region-p) (set-mark (point))))

		  (defun kakoune-deactivate-mark ()
		    "Deactivate the mark.
		  Deactivate the mark unless mark-region-mode is active."
		    (interactive)
		    ;;(unless rectangle-mark-mode (deactivate-mark)))
		    (deactivate-mark))

	  (defun kakoune-O (count)
	    "Open COUNT lines above the cursor and go into insert mode."
	    (interactive "p")
	    (beginning-of-line)
	    (dotimes (_ count)
	      (newline)
	      (forward-line -1)))

		  (defun kakoune-x (count)
		    "Select COUNT lines from the current line.
		  Note that kakoune's x does behaves exactly like this,
		  and I like this behavior better."
		    (interactive "p")
		    (beginning-of-line count)
		    (set-mark (point))
		    (end-of-line count)
		    (forward-char count))

		  (defun kakoune-d (count)
		    "Kill selected text or COUNT chars."
		    (interactive "p")
		    (if (use-region-p)
			(kill-region (region-beginning) (region-end))
		      (delete-char count t)))

	  (defun kakoune-insert-mode () "Return to insert mode."
			 (interactive)
			 (ryo-modal-mode 0))

	  (defun kakoune-normal-mode () "Return to insert mode."
			 (interactive)
			 (ryo-modal-mode 1))

		  (defun ryo-after () "Enter normal mode"
			 (interactive)
			    (forward-char)
			    (ryo-modal-mode 0))

	    (defun kakoune-X (count)
	      "Extend COUNT lines from the current line."
	      (interactive "p")
	      (beginning-of-line)
	      (unless (use-region-p) (set-mark (point)))
	      (forward-line count))

	    (defun kakoune-T (count)
	      "Extend COUNT lines from the current line."
	      (interactive "p")
	      (beginning-of-line)
	      (unless (use-region-p) (set-mark (point)))
	      (previous-line count))

	    (defun kakoune-gg (count)
		    "Go to the beginning of the buffer or the COUNTth line."
		    (interactive "p")
		    (goto-char (point-min))
		    (when count (forward-line (1- count))))

		  (defun kakoune-o (count)
		    "Open COUNT lines under the cursor and go into insert mode."
		    (interactive "p")
		    (end-of-line)
		    (dotimes (_ count)
		      (electric-newline-and-maybe-indent))
		    (ryo-modal-mode 0))

		  (defun kakoune-set-mark-here () "Set the mark at the location of the point."
			 (interactive) (set-mark (point)))

		  (defun kakoune-p (count)
		    "Yank COUNT times after the point."
		    (interactive "p")
		    (forward-line)
		    (dotimes (_ count) (save-excursion (yank))))

		  (defun kakoune-P (count)
		    "Yank COUNT times after the point."
		    (interactive "p")
		    (dotimes (_ count) (save-excursion (yank))))

	  (defvar kakoune-last-t-or-f ?f
	    "Using t or f command sets this variable.")

	  (defun kakoune-select-to-char (arg char)
	    "Select up to, and including ARGth occurrence of CHAR.
	  Case is ignored if `case-fold-search' is non-nil in the current buffer.
	  Goes backward if ARG is negative; error if CHAR not found.
	  Ignores CHAR at point."
	    (interactive "p\ncSelect to char: ")
	    (setq kakoune-last-char-selected-to char)
	    (setq kakoune-last-t-or-f ?f)
	    (let ((direction (if (>= arg 0) 1 -1)))
	      (forward-char direction)
	      (unwind-protect
		      (search-forward (char-to-string char) nil nil arg))
	      (point)))

	  (defun kakoune-select-up-to-char (arg char)
	    "Select up to, but not including ARGth occurrence of CHAR.
	  Case is ignored if `case-fold-search' is non-nil in the current buffer.
	  Goes backward if ARG is negative; error if CHAR not found.
	  Ignores CHAR at point."
	    (interactive "p\ncSelect up to char: ")
	    (setq kakoune-last-char-selected-to char)
	    (setq kakoune-last-t-or-f ?t)
	    (let ((direction (if (>= arg 0) 1 -1)))
	      (forward-char direction)
	      (unwind-protect
		      (search-forward (char-to-string char) nil nil arg)
		    (backward-char direction))
	      (point)))

		  (defun kakoune-A (count)
		    "Yank COUNT times after the point."
		    (interactive "p")
		    (end-of-line)
		    (ryo-modal-mode 0))

		  (use-package expand-region
		    :straight t
		    :bind ("C-+" . er/expand-region))

		  (use-package ryo-modal
		    :straight t
		    :commands ryo-modal-mode
		    :bind ("C-a" . ryo-modal-mode)
		    :config
		    (ryo-modal-keys
		     ("<RET>" next-line)
		     (";" kakoune-deactivate-mark)
		     ("0" beginning-of-line )
		     ("<backspace>" backward-char :first '(kakoune-deactivate-mark) )
		     ("%" mark-whole-buffer)
		     ("," ryo-modal-repeat)
		     ("/" consult-line)
		     ("C-=" text-scale-increase)   
		     ("C--" text-scale-decrease)
		     ("a" ryo-after)
		     ("b" backward-word :first '(kakoune-set-mark-here ))
		     ("c" comment-line)
		     ("d" kakoune-d)
		     ("f" kakoune-select-to-char :first '(kakoune-set-mark-here ))
		     ("h" backward-char :first '(kakoune-deactivate-mark))
		     ("i" kakoune-insert-mode)
		     ("j" next-line :first '(kakoune-deactivate-mark ))
		     ("k" previous-line :first '(kakoune-deactivate-mark))
		     ("l" forward-char :first '(kakoune-deactivate-mark  ))
		     ("o" kakoune-o)
		     ("p" kakoune-p)
		     ;;("q" ryo-modal-mode)
		     ("s" save-buffer)
		     ("t" kakoune-select-up-to-char :first '(kakoune-set-mark-here ))
		     ("w" forward-word :first '(kakoune-set-mark-here))
		     ("x" kakoune-x)
		     ("y" kill-ring-save)
		     ("A" kakoune-A)
		     ("B" backward-word :first '(kakoune-set-mark-if-inactive))
		     ("H" backward-char :first '(kakoune-set-mark-if-inactive))
		     ("J" next-line :first '(kakoune-set-mark-if-inactive))
		     ("K" previous-line :first '(kakoune-set-mark-if-inactive))
		     ("L" forward-char :first '(kakoune-set-mark-if-inactive))
		     ("M-w" forward-symbol :first '(kakoune-set-mark-here))
		     ("O" kakoune-O)
		     ("P" kakoune-P)
		     ("T" kakoune-T)
		     ("W" forward-word :first '(kakoune-set-mark-if-inactive))
		     ("X" kakoune-X)
		  )

		    (ryo-modal-keys
		     ;; First argument to ryo-modal-keys may be a list of keywords.
		     ;; These keywords will be applied to all keybindings.
		     (:norepeat t)
		     ;;("0" "M-0")
		     ("1" "M-1")
		     ("2" "M-2")
		     ("3" "M-3")
		     ("4" "M-4")
		     ("5" "M-5")
		     ("6" "M-6")
		     ("7" "M-7")
		     ("8" "M-8")
		     ("7" "M-7")
		     ("9" "M-9")))

		  ;; general

		;;  (ryo-modal-key
		;;   "C" '(("s" save-buffer)))
		  (ryo-modal-key
		   "SPC" '(
			   ;;("a" consult-line)
			   ("b b" consult-buffer)
			   ("b k" previous-buffer)
			   ("b j" next-buffer)
			   ("f" find-file)
			   ("g" magit-status)
			   ("h f" describe-function)
			   ("h k" describe-key)
			   ;; ("j" next-buffer)
			   ;; ("k" previous-buffer)
			   ("q" kill-buffer-and-window)
			   ("s" consult-line);;save-buffer)
			   ("r" reload)
			   ("w a" delete-other-windows)
			   ("w s" split-window-below)
			   ("w v" split-window-right)
			   ("w w" other-window)
			   ("t" consult-theme)
			   ("SPC" execute-extended-command)
			   ("<tab> n" tab-bar-new-tab-to )
			   ("<tab> d" tab-bar-close-tab)
			   ("<tab> b" switch-to-buffer-other-tab)
			   ("<tab> f" find-file-other-tab)
			   ("<tab> a" tab-bar-close-other-tabs )
			   ;; ("<tab> F" dired-other-tab)
			  ))

      ;; (ryo-modal-key "h" '(("r" haskell-process-reload)))

		  (ryo-modal-key
		   "g" '(("h" beginning-of-line)
			   ("j" end-of-buffer)
			   ("g" kakoune-gg)
			   ("l" end-of-line)
			   ("k" beginning-of-buffer)))

   (defun haskellCode ()
		   (interactive )
		    (ryo-modal-key "SPC" '(
					   ("c r" haskell-process-reload)
					   ("c l" haskell-process-load-file)
					   ("c c" haskell-interactive-switch)
					   )
  ))
 (defun haskellCodeRepl ()
		   (interactive )
		    (ryo-modal-key "SPC" '(
					   ;;("c r" haskell-process-reload)
					   ;;("c l" haskell-process-load-file)
					   ("c c" haskell-interactive-switch-back)
					   )
  ))
(add-hook 'haskell-mode-hook #'haskellCode)
(add-hook 'haskell-interactive-mode-hook #'haskellCodeRepl)
	;; (global-set-key (kbd "<escape>") 'kakoune-normal-mode)
	      (add-hook 'prog-mode-hook #'kakoune-normal-mode)
	      (add-hook 'org-mode-hook #'kakoune-normal-mode)

    ;; (add-hook 'interactive-haskell-mode-hook 'ac-haskell-process-setup)
    (add-hook 'haskell-interactive-mode-hook #'kakoune-normal-mode)
  (ryo-modal-mode 1)

(setq tab-bar-new-tab-choice "*scratch*")     
       (setq tab-bar-show nil)

(electric-pair-mode)

(use-package undo-tree
:straight t
:config
(global-undo-tree-mode)
:ryo
("u" undo-tree-undo)
("U" undo-tree-redo)
("SPC u" undo-tree-visualize)
:bind (:map undo-tree-visualizer-mode-map
            ("h" . undo-tree-visualize-switch-branch-left)
            ("j" . undo-tree-visualize-redo)
            ("k" . undo-tree-visualize-undo)
            ("l" . undo-tree-visualize-switch-branch-right)))

(use-package doom-modeline
	  :straight t
	  :ensure t)

      (defun doomModeline () "Return to insert mode."
		     (interactive)
		     (doom-modeline-mode 1))
     ;;     (add-hook 'prog-mode-hook #'doomModeline)
	  (doom-modeline-mode 1)

    (setq doom-modeline-height 23)
	(setq-default header-line-format mode-line-format)

		      (straight-use-package 'hide-mode-line)
	(global-hide-mode-line-mode)

    ;;(set-face-attribute 'mode-line nil :font "Consolas" :height 170)  
    ;;(set-face-attribute 'mode-line-inactive nil :font "Consolas" :height 170)    ;; (setq-default mode-line-format nil)
    ;; (set-face-background 'modeline "#4477aa")
;; (display-battery-mode)

;; (display-time)

(use-package org-tempo
  :ensure nil)

;;(add-hook 'org-mode-hook 'org-indent-mode)

(setq org-ellipsis "⤵")

;;(use-package org-superstar
      ;; :straight t)
;;(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(add-hook 'org-mode-hook #'org-shifttab)

;;(use-package direnv
;; :straight t)

(use-package envrc
      :straight t)
(envrc-global-mode)

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(global-set-key (kbd "<escape>")  'kakoune-normal-mode)

(use-package vterm-toggle
    :straight t)

(use-package fzf
  :straight t)

;;(add-to-list 'org-latex-classes
;;             '("article"
;;               "\\documentclass{article}"
;;;               ("\\section{%s}" . "\\section*{%s}")
;;               ("\\subsection{%s}" . "\\subsection*{%s}")
;;               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
;;               ("\\paragraph{%s}" . "\\paragraph*{%s}")
;;               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(global-hl-line-mode 1)

(use-package nimbus-theme
   :straight t)
;;(load-theme 'nimbus t)

;;(use-package vscode-dark-plus-theme
;;  :straight t
;;  :ensure t
;;  :config
;;  (load-theme 'vscode-dark-plus t))

(use-package solaire-mode
  :straight t
  :ensure t
  :config
  (solaire-global-mode +1))

;;   (use-package gruvbox
;;     :straight t)

(use-package doom-themes
  :straight t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one t)
  (doom-themes-org-config))

;;(use-package epresent
 ;; :straight t)

(use-package org-tree-slide
  :straight t)

(use-package evil-snipe
   :straight t)
