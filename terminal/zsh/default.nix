{ config, ... }: {
  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableCompletion = true;
    enableSyntaxHighlighting = true;
    enableVteIntegration = true;
    autocd = true;
    shellAliases = {
      # ll = "ls -l";
      home-switch = ''
        nix run "/home/vamshi/config#cosmos" ''; # --impure && notify-send \"Successfully updated\" || notify-send -u critical \"Updating failed!!\" ";
      update = "sudo nixos-rebuild switch";
      del = "sudo nix-collect-garbage -d";
      wifi = "systemctl start wpa_supplicant-wlp2s0.service";
    };
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
    # zplug = {
    #   enable = true;
    # plugins = [
    # { name = "zsh-users/zsh-autosuggestions"; } # Simple plugin installation
    #   { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } # Installations with additional options. For the list of options, please refer to Zplug README.
    # ];
    # };
  };
}
