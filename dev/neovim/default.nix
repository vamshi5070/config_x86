{ pkgs, ... }: {
  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
    package = pkgs.neovim-nightly;
    plugins = with pkgs.vimPlugins; [ 
    gruvbox 
    lightline-vim 
    vim-nix
    telescope-nvim
    fugitive-gitlab-vim
    vim-fugitive
    ];
    extraConfig = ''
                      set relativenumber
      		colorscheme gruvbox
      		 syntax enable                             " Syntax highlighting
              let g:lightline = {
                \ 'colorscheme': 'wombat',
                \ }
    '';
  };
  #      colorscheme srcery                        " Color scheme text
}
