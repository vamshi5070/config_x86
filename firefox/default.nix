{ config, pkgs, ... }:

let
    # Import config files
    # zshsettings = import ./zsh/zsh.nix;
    # nvimsettings = import ./nvim/nvim.nix;
    ffsettings = import ./firefox.nix;
in
{
    # Enable home-manager
    # programs.home-manager.enable = true;

    # Source extra files that are too big for this one
    # programs.zsh = zshsettings pkgs;
    # programs.neovim = nvimsettings pkgs;
    programs.firefox = ffsettings pkgs;
}
